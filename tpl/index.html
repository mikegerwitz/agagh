
    <div class="jumbotron">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <h1>A Guide for the Aspiring GNU/Linux Hacker</h1>
            <p>
              Learn your way around a <a href="https://www.gnu.org/">GNU/Linux</a>
              system from the perspective of an aspiring
              <a href="https://www.gnu.org/philosophy/words-to-avoid.html#Hacker">hacker</a>
              looking to grok and playfully explore the operating system as both a
              development environment and a home.
            </p>
            <p>
            <a href="concept.html" class="btn btn-primary btn-lg" role="button">
              Start hacking &raquo;
            </a>
              <a href="https://www.gnu.org/"
                class="btn btn-default btn-lg pull-right"
                role="button">
                  What is GNU/Linux?
              </a>
            </p>
          </div>
          <div class="col-lg-4 homebig">
            <img src="https://www.gnu.org/graphics/heckert_gnu.svg" alt="GNU"
              class="" />
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <h2>Introduction</h2>
          <p>
            This guide provides useful information to hackers of all skill
            levels: discover this guide's target audience and what it can do
            for you. Not interested in the hacker ideology? No problem;
            you're still invited.
          </p>
          <p><a class="btn btn-default" href="#" role="button">Start hacking &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2>Files and Paths</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">Start hacking &raquo;</a></p>
       </div>
        <div class="col-md-4">
          <h2>Service Administration</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="#" role="button">Start hacking &raquo;</a></p>
        </div>
      </div>

      <hr />

      <div class="container">
        <h2 class="text-center" id="about">About This Guide</h2>
        <div class="row about about-learn">
          <div class="col-md-5 col-md-push-7 icon-huge">
            <span class="glyphicon glyphicon-pencil"></span>
          </div>
          <div class="col-md-7 col-md-pull-5">
            <h3>
              Learn Through Engagement.
              <span class="text-muted">
                Use critical thought and repetition to encourage retention.
              </span>
            </h3>
            <p>
              Mastery comes with experience&mdash;lots of it. Unfortunately,
              readers tend to treat informal texts casually and may not try
              out the code/concepts as they read along (perhaps some are not
              even sitting at their computers). The format of this guide is
              motivated heavily by <a href="http://www.ccs.neu.edu/home/matthias/BTLS/">The Little Schemer</a>,
              which encourages the development of essential intuition by
              provoking creative thought from readers by engaging in
              constant dialog and questioning; readers may study up
              and&mdash;having committed it to memory&mdash;try out the
              principles on a computer at their earliest convenience.
            </p>
            <p>
              <small class="text-muted">
                <i>(The Little Schemer makes for an excellent bedtime story.)</i>
              </small>
            </p>
          </div>
        </div>

        <div class="row about about-excite">
          <div class="col-md-5 icon-huge">
            <span class="glyphicon glyphicon-heart-empty"></span>
          </div>
          <div class="col-md-7">
            <h3>
              Engagement Is Excitement.
              <span class="text-muted">
                Keeping readers engaged keeps them motivated to learn more.
              </span>
            </h3>
            <p>
              Do you find the topics presented in this guide (or hacking in
              general) to be more fascinating than any movie, novel, or
              musical composition? Me too; but not everyone shares those
              feelings&mdash;this guide is for hackers and non-hackers alike
              (though the former will certainly find more enjoyment).
              Sometimes, learning is part of the job (or strongly encouraged
              by a coworker/colleague), in which case we want to be able to
              provide an atmosphere that is <em>fun</em> to learn in; this
              guide does so by actively engaging the user in both the text
              itself, and in goals and features surrounding it.
            </p>
            <p>
              <small class="text-muted">
                (Though
                <a href="http://en.wikipedia.org/wiki/G%C3%B6del,_Escher,_Bach">some
                  great minds of the past can certainly be considered
                  hackers</a>
                in their own right&mdash;so let us be careful to not deprive
                ourselves of such pleasures.)
              </small>
            </p>
          </div>
        </div>


        <div class="row about about-theory">
          <div class="col-md-5 col-md-push-7 icon-huge">
            <span class="glyphicon glyphicon-tower"></span>
          </div>
          <div class="col-md-7 col-md-pull-5">
            <h3>
              Practical with a Sprinkle of Theory.
              <span class="text-muted">
                Provide knowledge that readers will actually <em>use</em>,
                while still feeding the hungry minds looking to grow even
                more.
              </span>
            </h3>
            <p>
              Have you ever sat through a school course wondering to
              yourself: &ldquo;damnit; when am I ever going to use the
              Pythagorean Theorem?!&rdquo; (or any other topic)? Did your
              teacher provide a practical foundation demonstrating
              applicability to the &ldquo;real world&rdquo; before mounting
              theory? What about providing the wonderfully intriguing history of <a
                href="https://en.wikipedia.org/wiki/Pythagoras">Pythagoras
                and the Pythagoreans</a>? What about the fascinating story
              of <a href="http://en.wikipedia.org/wiki/Hippasus">Hippasus of
                Metapontum</a>&mdash;a Pythagorean philosopher&mdash;being
              banished and drown at sea as punishment for demonstrating the
              existence of irrational numbers? Perhaps, even if you were aware
              of all of this, you may not have been interested at the time.
            </p>
            <p>
              It is important to put knowledge learned into perspective so that
              there is a clear foundation from which to grow&mdash;an intuition
              to which theory can be applied. For those interested only in practical
              knowledge, it can be found here. For those excited about all the details
              that they may never use (or maybe they will!)&mdash;or for those who
              one day do gain interest&mdash;well, we have you covered too.
            </p>
            <p>
              <small class="text-muted">
                (On that note, understanding the
                <a href="http://www.goodreads.com/book/show/12150875-a-strange-wilderness">history
                  of mathematics</a>
                can help to demystify it&mdash;and concepts related to programming.)
              </small>
            </p>
          </div>
        </div>

        <div class="row about about-philosophy">
          <div class="col-md-5 icon-huge">
            <span class="glyphicon glyphicon-globe"></span>
          </div>
          <div class="col-md-7">
            <h3>
              Philosophy of Sharing; Learning; Growing.
              <span class="text-muted">
                The Free Software philosophy is exemplified in this guide.
              </span>
            </h3>
            <p>
              When a friend needs help, is your first instinct to share your knowledge and provide
              aid, or to tell him or her, &ldquo;sorry; you&rsquo;re on your own&rdquo;? The latter
              is the <a href="https://www.gnu.org/philosophy/free-software-even-more-important.html">approach
                of proprietary software</a>&mdash;developers exhort control over users, telling you
              <a href="https://www.gnu.org/philosophy/proprietary-drm.html">what you can and cannot do
                with your own computer and devices</a>; this is an approach that is wholly
              <a href="https://www.gnu.org/education/education.html">incompatible with education</a>.
            </p>
            <p>
              The <a href="https://www.gnu.org/philosophy/free-sw.html">Free Software movement</a>
              argues that <a href="https://www.gnu.org/philosophy/shouldbefree.html">all software should
                be Free</a> (as in &ldquo;free speech&rdquo;, not &ldquo;free beer&rdquo;); it encourages
              helping and <em>empowering</em> your friends and neighbors. This guide encourages this
              philosophy by example, by encouraging only the use of Free Software, and by demonstrating
              the merits of the philosophy at every turn.
            </p>
            <p>
              <small class="text-muted">
                (With software moving increasingly toward the web,
                <a href="https://www.gnu.org/software/easejs/whyfreejs.html">do not yet the illusion
                  of remote execution fool you</a>&mdash;this software must still be Free.)
              </small>
            </p>
          </div>
        </div>
      </div>
    </div>

