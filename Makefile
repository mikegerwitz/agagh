
webroot := webroot
webroot_css := $(webroot)/style
webroot_js := $(webroot)/scripts
webroot_fonts := $(webroot)/fonts

tpl_head := tpl/_header.html
tpl_foot := tpl/_footer.html

# packages installed with bower
bower_root := bower_components

# bootstrap
bootstrap_root := $(bower_root)/bootstrap
bootstrap_js := $(bootstrap_root)/dist/js/bootstrap.min.js
bootstrap_glyph_font := $(bootstrap_root)/fonts/glyphicons-halflings-regular.woff

# bootswatch
bootswatch_root := $(bower_root)/bootswatch
theme := $(bootswatch_root)/darkly
theme_css := $(theme)/bootstrap.min.css

# files required for producing working webroot
web_defaults := $(webroot) \
                $(webroot)/index.html \
                $(webroot)/concept.html \
                $(webroot_css)/bootstrap.min.css \
                $(webroot_css)/aspire.css \
                $(webroot_js)/bootstrap.min.js \
                $(webroot_fonts)/glyphicons-halflings-regular.woff

.PHONY: default clean

default: $(web_defaults)


$(webroot)/%.html: tpl/%.html $(tpl_head) $(tpl_foot) $(webroot)
	cat "$(tpl_head)" "tpl/$*.html" "$(tpl_foot)" > "$@"


# for now, use precompiled style (TODO: we won't need everything)
$(webroot_css)/bootstrap.min.css: $(webroot_css)
	cp -v "$(theme_css)" "$@"

$(webroot_css)/%.css: style/%.css $(webroot_css)
	cp -v "$<" "$@"

# TODO: we do not need every module included in this
$(webroot_js)/bootstrap.min.js: $(webroot_js)
	cp -v "$(bootstrap_js)" "$@"

$(webroot_fonts)/glyphicons-halflings-regular.woff: $(webroot_fonts)
	cp -v "$(bootstrap_glyph_font)" "$@"

$(webroot):
	mkdir -vp "$@"
$(webroot)/%:
	mkdir -vp "$@"

clean:
	$(RM) -r $(webroot)

